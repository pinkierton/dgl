#version 420 core

in vec2 position;
in vec3 color;

out vec3 Color;

void main()
{
    Color = color;
    gl_Position.xy = position;
    gl_Position.z = 0.0;
    gl_Position.w = 0.5;
//    gl_Position = vec4(position, 0.0, 1.0);
}
