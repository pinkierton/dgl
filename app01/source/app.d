import std.stdio;
import std.exception;
import derelict.glfw3;
import derelict.opengl3.gl3;
import std.string;
import std.format;
import std.datetime;
import core.thread;
import std.math;

GLuint compileShader(GLuint type, string sourceFile)(){
    static assert((type == GL_VERTEX_SHADER) || (type == GL_FRAGMENT_SHADER) || (type == GL_GEOMETRY_SHADER));

    static const GLchar *shaderSource = import(sourceFile);

    GLuint shader = glCreateShader(type);
    glShaderSource(shader, 1, &shaderSource, null);
    glCompileShader(shader);
    GLint status;
    glGetShaderiv(shader, GL_COMPILE_STATUS, &status);
    if (status != GL_TRUE) {
        char[512] buffer;
        glGetShaderInfoLog(shader, 512, null, buffer.ptr);
        throw new Exception(format("shader compilation error: %s", buffer));
    }

    return shader;

}

void main()
{
    DerelictGL3.load();
    DerelictGLFW3.load();

    enforce(glfwInit(), "glfwInit() failed");
    scope(exit) glfwTerminate();

    // использовать OpenGL 4.5
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 5);

    // использовать профиль core, ископаемое говно не нужно
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);

    // запретить ресайз окна
    glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);

    auto window = glfwCreateWindow(640, 480, "Hello world", null, null);
    enforce(window, "glfwCreateWindow() failed");

    glfwMakeContextCurrent(window);
    DerelictGL3.reload();

    float[20] vertices = [
        -0.5f,  0.5f, 1.0f, 0.0f, 0.0f, // Top-left
         0.5f,  0.5f, 0.0f, 1.0f, 0.0f, // Top-right
         0.5f, -0.5f, 0.0f, 0.0f, 1.0f, // Bottom-right
        -0.5f, -0.5f, 1.0f, 1.0f, 1.0f  // Bottom-left
    ];

    GLuint[6] elements = [
        0, 1, 2,
        2, 3, 0
    ];

    GLuint vao;
    glGenVertexArrays(1, &vao);
    glBindVertexArray(vao);

    GLuint vbo;
    glGenBuffers(1, &vbo);
    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    glBufferData(GL_ARRAY_BUFFER, vertices.sizeof, vertices.ptr, GL_STATIC_DRAW);

    GLuint ebo;
    glGenBuffers(1, &ebo);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, elements.sizeof, elements.ptr, GL_STATIC_DRAW);

    GLuint vertexShader = compileShader!(GL_VERTEX_SHADER, "main.vert");
    GLuint fragmentShader = compileShader!(GL_FRAGMENT_SHADER, "main.frag");

    GLuint shaderProgram = glCreateProgram();
    scope(exit) glDeleteProgram(shaderProgram);

    glAttachShader(shaderProgram, vertexShader);
    scope(exit) glDeleteShader(vertexShader);

    glAttachShader(shaderProgram, fragmentShader);
    scope(exit) glDeleteShader(fragmentShader);

    glBindFragDataLocation(shaderProgram, 0, "outColor"); // не обязательно
    glLinkProgram(shaderProgram);
    glUseProgram(shaderProgram);

    GLint posAttrib = glGetAttribLocation(shaderProgram, "position");
    glEnableVertexAttribArray(posAttrib);
    glVertexAttribPointer(posAttrib, 2, GL_FLOAT, GL_FALSE, 5*GLfloat.sizeof, null);

    GLint colAttrib = glGetAttribLocation(shaderProgram, "color");
    glEnableVertexAttribArray(colAttrib);
    glVertexAttribPointer(colAttrib, 3, GL_FLOAT, GL_FALSE, 5*GLfloat.sizeof, cast(void*) (2*GLfloat.sizeof));

    // цикл пока пользователь не закроет окно
    while (!glfwWindowShouldClose(window)) {

        glClearColor(0.0, 0.0, 0.0, 1.0);
        glClear(GL_COLOR_BUFFER_BIT);

        //glDrawArrays(GL_TRIANGLES, 0, 3);
        glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, null);

        // переключить буффера - хз, двойная буфферизация
        glfwSwapBuffers(window);

        // обработка событий
        glfwPollEvents();
    }
}
