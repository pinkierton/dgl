#version 420 core

in vec3 Color;

out vec4 outColor;

void main()
{
    outColor.rgb = (dot(Color, vec3(1)) / 3.0).xxx;
    outColor.w = 1.0;
}
