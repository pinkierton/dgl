module soil;

enum {
    SOIL_LOAD_AUTO,
    SOIL_LOAD_L,
    SOIL_LOAD_LA,
    SOIL_LOAD_RGB,
    SOIL_LOAD_RGBA
};

extern (C) {
    ubyte* SOIL_load_image(const char* filename, int* width, int* height, int *channels, int force_channels);
    void SOIL_free_image_data(ubyte* img_data);
}