#version 450 core

in vec3 Color;
in vec2 Texcoord;

out vec4 outColor;

uniform sampler2D texCat;
uniform sampler2D texPony;
uniform float proportion;

void main()
{
    vec4 colCat = texture(texCat, Texcoord);
    vec4 colPony = texture(texPony, Texcoord);
    outColor = mix(colCat, colPony, proportion) * vec4(Color, 1.0);
};