import std.stdio;
import std.exception;
import derelict.glfw3;
import derelict.opengl3.gl3;
import std.string;
import std.format;
import std.datetime;
import core.thread;
import std.math;

import soil;
import gl3n.linalg;
import gl3n.math;

import std.algorithm.iteration;

import std.math;

import std.array;
import std.conv;

GLuint compileShader(GLuint type, string sourceFile)(){
    static assert((type == GL_VERTEX_SHADER) || (type == GL_FRAGMENT_SHADER) || (type == GL_GEOMETRY_SHADER));

    static const GLchar *shaderSource = import(sourceFile);

    GLuint shader = glCreateShader(type);
    glShaderSource(shader, 1, &shaderSource, null);
    glCompileShader(shader);
    GLint status;
    glGetShaderiv(shader, GL_COMPILE_STATUS, &status);
    if (status != GL_TRUE) {
        char[512] buffer;
        glGetShaderInfoLog(shader, 512, null, buffer.ptr);
        throw new Exception(format("shader compilation error: %s", buffer));
    }

    return shader;

}

// test redefinition of mat4.look_at
Matrix!(mt, 4, 4) lookAt(mt)(Vector!(mt, 3) eye, Vector!(mt, 3) target, Vector!(mt, 3) up) {
    alias Vector!(mt, 3) vec3mt;
    alias Vector!(mt, 4) vec4mt;
    alias Matrix!(mt, 4, 4) mat4mt;

    vec3mt f = (center - eye).normalized;
    vec3mt s = cross(f, up).normalized;
    vec3mt u = cross(s, f);

    return mat4mt(
        vec4mt(s, dot(-s, eye)),
        vec4mt(u, dot(-u, eye)),
        vec4mt(-f, dot(f, eye)),
        vec4mt(0,0,0,1)
    );
}

static const winWidth = 800;
static const winHeight = 600;

void main()
{
    MonoTime start = MonoTime.currTime();

    DerelictGL3.load();
    DerelictGLFW3.load();

    enforce(glfwInit(), "glfwInit() failed");
    scope(exit) glfwTerminate();

    // use OpenGL 4.5
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 5);

    // force modern OpenGL core profile
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);

    glfwWindowHint(GLFW_DEPTH_BITS, 24);
    glfwWindowHint(GLFW_STENCIL_BITS, 8);

    // window resize is prohibited
    glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);

    auto window = glfwCreateWindow(winWidth, winHeight, "to the gates of hell", null, null);
    enforce(window, "glfwCreateWindow() failed");

    glfwMakeContextCurrent(window);
    DerelictGL3.reload();

    static const GLfloat[288] vertices = [
        -0.5f, -0.5f, -0.5f, 1.0f, 1.0f, 1.0f, 0.0f, 0.0f,
         0.5f, -0.5f, -0.5f, 1.0f, 1.0f, 1.0f, 1.0f, 0.0f,
         0.5f,  0.5f, -0.5f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f,
         0.5f,  0.5f, -0.5f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f,
        -0.5f,  0.5f, -0.5f, 1.0f, 1.0f, 1.0f, 0.0f, 1.0f,
        -0.5f, -0.5f, -0.5f, 1.0f, 1.0f, 1.0f, 0.0f, 0.0f,

        -0.5f, -0.5f,  0.5f, 1.0f, 1.0f, 1.0f, 0.0f, 0.0f,
         0.5f, -0.5f,  0.5f, 1.0f, 1.0f, 1.0f, 1.0f, 0.0f,
         0.5f,  0.5f,  0.5f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f,
         0.5f,  0.5f,  0.5f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f,
        -0.5f,  0.5f,  0.5f, 1.0f, 1.0f, 1.0f, 0.0f, 1.0f,
        -0.5f, -0.5f,  0.5f, 1.0f, 1.0f, 1.0f, 0.0f, 0.0f,

        -0.5f,  0.5f,  0.5f, 1.0f, 1.0f, 1.0f, 1.0f, 0.0f,
        -0.5f,  0.5f, -0.5f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f,
        -0.5f, -0.5f, -0.5f, 1.0f, 1.0f, 1.0f, 0.0f, 1.0f,
        -0.5f, -0.5f, -0.5f, 1.0f, 1.0f, 1.0f, 0.0f, 1.0f,
        -0.5f, -0.5f,  0.5f, 1.0f, 1.0f, 1.0f, 0.0f, 0.0f,
        -0.5f,  0.5f,  0.5f, 1.0f, 1.0f, 1.0f, 1.0f, 0.0f,

         0.5f,  0.5f,  0.5f, 1.0f, 1.0f, 1.0f, 1.0f, 0.0f,
         0.5f,  0.5f, -0.5f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f,
         0.5f, -0.5f, -0.5f, 1.0f, 1.0f, 1.0f, 0.0f, 1.0f,
         0.5f, -0.5f, -0.5f, 1.0f, 1.0f, 1.0f, 0.0f, 1.0f,
         0.5f, -0.5f,  0.5f, 1.0f, 1.0f, 1.0f, 0.0f, 0.0f,
         0.5f,  0.5f,  0.5f, 1.0f, 1.0f, 1.0f, 1.0f, 0.0f,

        -0.5f, -0.5f, -0.5f, 1.0f, 1.0f, 1.0f, 0.0f, 1.0f,
         0.5f, -0.5f, -0.5f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f,
         0.5f, -0.5f,  0.5f, 1.0f, 1.0f, 1.0f, 1.0f, 0.0f,
         0.5f, -0.5f,  0.5f, 1.0f, 1.0f, 1.0f, 1.0f, 0.0f,
        -0.5f, -0.5f,  0.5f, 1.0f, 1.0f, 1.0f, 0.0f, 0.0f,
        -0.5f, -0.5f, -0.5f, 1.0f, 1.0f, 1.0f, 0.0f, 1.0f,

        -0.5f,  0.5f, -0.5f, 1.0f, 1.0f, 1.0f, 0.0f, 1.0f,
         0.5f,  0.5f, -0.5f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f,
         0.5f,  0.5f,  0.5f, 1.0f, 1.0f, 1.0f, 1.0f, 0.0f,
         0.5f,  0.5f,  0.5f, 1.0f, 1.0f, 1.0f, 1.0f, 0.0f,
        -0.5f,  0.5f,  0.5f, 1.0f, 1.0f, 1.0f, 0.0f, 0.0f,
        -0.5f,  0.5f, -0.5f, 1.0f, 1.0f, 1.0f, 0.0f, 1.0f
    ];

    static const GLuint[18] elements = [
        0, 1, 2,
        2, 3, 0,
        7, 4, 5,
        5, 6, 7,
        7, 3, 2,
        2, 6, 7
    ];

    GLuint vao;
    glGenVertexArrays(1, &vao);
    glBindVertexArray(vao);

    GLuint vbo;
    glGenBuffers(1, &vbo);
    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    glBufferData(GL_ARRAY_BUFFER, vertices.sizeof, vertices.ptr, GL_STATIC_DRAW);

    GLuint ebo;
    glGenBuffers(1, &ebo);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, elements.sizeof, elements.ptr, GL_STATIC_DRAW);



    GLuint vertexShader = compileShader!(GL_VERTEX_SHADER, "main.vert");
    GLuint fragmentShader = compileShader!(GL_FRAGMENT_SHADER, "main.frag");

    GLuint shaderProgram = glCreateProgram();
    scope(exit) glDeleteProgram(shaderProgram);

    glAttachShader(shaderProgram, vertexShader);
    scope(exit) glDeleteShader(vertexShader);

    glAttachShader(shaderProgram, fragmentShader);
    scope(exit) glDeleteShader(fragmentShader);

    glBindFragDataLocation(shaderProgram, 0, "outColor"); // is not necessary
    glLinkProgram(shaderProgram);
    glUseProgram(shaderProgram);

    GLint posAttrib = glGetAttribLocation(shaderProgram, "position");
    glEnableVertexAttribArray(posAttrib);
    glVertexAttribPointer(posAttrib, 3, GL_FLOAT, GL_FALSE, 8 * GLfloat.sizeof, null);

    GLint colAttrib = glGetAttribLocation(shaderProgram, "color");
    glEnableVertexAttribArray(colAttrib);
    glVertexAttribPointer(colAttrib, 3, GL_FLOAT, GL_FALSE, 8 * GLfloat.sizeof, cast(void*)(3 * GLfloat.sizeof));

    GLint texAttrib = glGetAttribLocation(shaderProgram, "texcoord");
    glEnableVertexAttribArray(texAttrib);
    glVertexAttribPointer(texAttrib, 2, GL_FLOAT, GL_FALSE, 8 * GLfloat.sizeof, cast(void*)(6 * GLfloat.sizeof));


    // Load texture
    GLuint[2] textures;
    glGenTextures(2, textures.ptr);

    int width, height;
    ubyte* image;

    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, textures[0]);
    image = SOIL_load_image("source/pic1.png", &width, &height, null, SOIL_LOAD_RGB);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, image);
    SOIL_free_image_data(image);
    glUniform1i(glGetUniformLocation(shaderProgram, "texCat"), 0);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    glActiveTexture(GL_TEXTURE1);
    glBindTexture(GL_TEXTURE_2D, textures[1]);
    image = SOIL_load_image("source/pic2.png", &width, &height, null, SOIL_LOAD_RGB);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, image);
    SOIL_free_image_data(image);
    glUniform1i(glGetUniformLocation(shaderProgram, "texPony"), 1);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    GLint mixProportion = glGetUniformLocation(shaderProgram, "proportion");
    GLint uniModel = glGetUniformLocation(shaderProgram, "model");
    GLint uniProj = glGetUniformLocation(shaderProgram, "proj");
    GLint uniView = glGetUniformLocation(shaderProgram, "view");

    mat4 view = mat4.look_at(
        27*vec3(1.2f, 1.2f, 0.98f),
        vec3(0.0f, 0.0f, 0.0f),
        vec3(0.0f, 0.0f, 1.0f)
    );
    glUniformMatrix4fv(uniView, 1, GL_TRUE, view.value_ptr);

    mat4 proj = mat4.perspective(winWidth, winHeight, PI/4, 1, 10);
    glUniformMatrix4fv(uniProj, 1, GL_FALSE, proj.value_ptr);

    glEnable(GL_DEPTH_TEST);

    // wait until user closes window
    while (!glfwWindowShouldClose(window)) {

        glClearColor(0.0, 0.0, 0.0, 1.0);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        float elapsed = (MonoTime.currTime() - start).total!"msecs";

        float t = (sin(elapsed/1000) + 1.0f) / 2.0f;
        glUniform1f(mixProportion, t);

        mat4 model = mat4.zrotation((elapsed/11).radians);
        glUniformMatrix4fv(uniModel, 1, GL_FALSE, model.value_ptr);

        //glDrawElements(GL_TRIANGLES, elements.length, GL_UNSIGNED_INT, null);
        glDrawArrays(GL_TRIANGLES, 0, 36);

        glfwSwapBuffers(window);

        glfwPollEvents();
    }
}
